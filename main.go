package main

import (
	"fmt"
	"./conf"
)

func main()  {

	fmt.Println("test conf")

	testINI()
	testJSON()
	testXML()
	testYAML()

	testJsonStruct()
	testXmlStruct()
}

func testINI()  {

	fmt.Println("-----------------------------------------")
	fmt.Println("ini")

	i := conf.New("./conf.ini")
	fmt.Println(i)
	fmt.Println(i.Get("server", "host"))
}

func testJSON()  {

	fmt.Println("-----------------------------------------")
	fmt.Println("json")

	j := conf.New("./conf.json")
	fmt.Println(j)
	fmt.Println(j.Get("server", "host"))
}

func testJsonStruct()  {

	fmt.Println("-----------------------------------------")
	fmt.Println("json struct")

	type Server struct {
		Host string 	`json:"host"`
		Port int 		`json:"port"`
		Dbs  []int 		`json:"dbs"`
	}
	
	type Config struct {
		Debug    bool   `json:"debug"`
		Server   Server `json:"server"`
	}

	config := &Config{}
	conf.NewJson("./conf.json", config)

	fmt.Println(config)
}

func testXML()  {

	fmt.Println("-----------------------------------------")
	fmt.Println("xml")

	x := conf.New("./conf.xml")
	fmt.Println(x)
	fmt.Println(x.Get("config", "server", "host"))

}

func testXmlStruct()  {
	fmt.Println("-----------------------------------------")
	fmt.Println("xml struct")

	type Server struct {
		Host string 	`xml:"host"`
		Port int 		`xml:"port"`
		Dbs  string 	`xml:"dbs"`
	}

	type Config struct {
		Debug    bool   `xml:"debug"`
		Server   Server `xml:"server"`
	}

	config := &Config{}
	conf.NewXml("./conf.xml", config)

	fmt.Println(config)
}

func testYAML()  {

	fmt.Println("-----------------------------------------")
	fmt.Println("yaml")

	y := conf.New("./conf.yaml")
	fmt.Println(y)
	fmt.Println(y.Get("server", "master", "host"))
}